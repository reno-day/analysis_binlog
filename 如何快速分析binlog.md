需求：某业务MySQL迁移，但是迁移前需要做如下准备工作。

- 1、统计各个业务表的dml操作情况。
- 2、统计各个业务表的最后访问时间。

条件：

- 1、60min一个1GB的binlog。
- 2、binlog保留一个月。


如果你遇到这么个需求，你该如何着手分析呢？反正我面对这个需求的时候第一个想到的就是写脚本，让脚本自动分析，下面分享一下我的分析过程：


1、安装分析binlog脚本
```
git clone https://gitee.com/mo-shan/analysis_binlog.git
cd analysis_binlog
sed -i 's#mysqlbinlog=.*#mysqlbinlog=\"/usr/local/mysql/bin/mysqlbinlog\"#g' bin/analysis_binlog    #配置mysqlbinlog工具的路径，否则可能会因版本太低导致错误 必须配置
sed -i 's#work_dir=.*#work_dir=\"/home/moshan/analysis_binlog\"#g' bin/analysis_binlog              #配置analysis_binlog家目录 必须配置
chmod +x bin/analysis_binlog                                                                        #添加可执行权限
echo "export PATH=/home/moshan/analysis_binlog/bin:${PATH}" >> ${HOME}/.bashrc                      #配置环境变量
```

2、根据需求执行
- -bfile: 指定binlog文件, 支持多个文件并行分析, 多个文件用逗号相隔, 需要并行分析时请结合-w参数使用
- -w    : 指定并行数, 当需要分析多个binlog文件时该参数有效, 默认是1
- -t    : 指定显示结果的格式/内容, 供选选项有"detail|simple". 当指定detail的时候结果较为详细, 会打印详细的分析过程, 消耗时间也不直观, simple只做了统计工作
- -s    : 指定排序规则, 供选选项有"insert|update|delete". 默认会把统计结果做一个排序, 按照表的维度统计出insert update delete的次数, 并按照次数大小排序(默认insert)
>注: 其他参数使用请参见帮助手册 bash analysis_binlog -h

```
analysis_binlog -bfile=/data/mysql/binlog/3306/mysql-bin.000798,/data/mysql/binlog/3306/mysql-bin.000799 -w=2 -t=simple -s=update  
```

3、结果查询

分析完毕会在analysis_binlog家目录下的res目录下保存一个[binlog_file_name.res]文件，使用文本工具打开即可, 建议使用cat, tail, more, 如下结果展示, 会按照表的维度做个统计, 然后按照update的次数排序, Last Time表示该表的最后一次操作
```
root /data/git/analysis_binlog/res >> cat mysql-bin.000798.res
Table                                                       Last Time                     Insert(s)      Update(s)      Delete(s)      
moshan.flush_                                               190311 9:28:54                0              3475           0              
ultrax.dis_common_syscache                                  190312 11:31:53               0              231            0              
ultrax.dis_common_cron                                      190312 11:31:53               0              194            0              
ultrax.dis_common_session                                   190312 10:38:56               6              170            5              
ultrax.dis_forum_forum                                      190312 9:19:10                0              129            0              
moshan.money                                                190311 9:28:37                29             80             0              
ultrax.dis_common_onlinetime                                190312 10:38:42               0              48             0              
ultrax.dis_forum_thread                                     190312 10:38:56               4              47             0              
ultrax.dis_common_member_count                              190312 10:38:53               0              47             0              
ultrax.dis_common_credit_rule_log                           190312 10:38:53               0              38             0              
ultrax.dis_forum_post                                       190312 9:24:30                4              34             0              
ultrax.dis_common_member_status                             190312 9:04:42                0              20             0              
moshan.history_                                             190308 9:28:25                0              10             0              
ice_db.server_setting_tmp                                   190304 10:34:19               564            8              0              
ultrax.dis_common_process                                   190312 11:31:53               201            7              201            
ultrax.dis_common_setting                                   190312 9:04:42                0              7              0              
moshan.tmp_table                                            190304 17:17:21               0              7              0              
ultrax.dis_ucenter_failedlogins                             190306 10:07:11               0              4              0              
ultrax.dis_common_member_field_home                         190311 14:54:47               0              4              0              
ultrax.dis_forum_threadcalendar                             190312 9:09:56                2              2              0              
ultrax.dis_forum_attachment                                 190306 11:46:56               2              2              0              
moshan.use_date                                             190304 17:12:22               0              1              0              
ultrax.dis_forum_threadhot                                  190312 9:09:56                4              0              0              
ultrax.dis_forum_threaddisablepos                           190311 14:54:47               1              0              0              
ultrax.dis_forum_statlog                                    190312 9:04:42                304            0              0              
ultrax.dis_forum_sofa                                       190311 14:54:47               4              0              0              
ultrax.dis_forum_post_tableid                               190311 14:54:47               4              0              0              
ultrax.dis_forum_newthread                                  190311 14:54:47               4              0              6              
ultrax.dis_forum_attachment_unused                          190306 11:46:56               2              0              2              
ultrax.dis_forum_attachment_8                               190306 11:46:56               1              0              0              
ultrax.dis_forum_attachment_0                               190306 11:46:29               1              0              0              
ultrax.dis_common_statuser                                  190311 11:40:44               4              0              4              
ultrax.dis_common_searchindex                               190312 10:38:53               28             0              0              
ultrax.dis_common_member_action_log                         190311 14:54:47               4              0              4              
test.ttt                                                    190303 11:43:36               2              0              0              
test.t_test                                                 190308 16:52:35               4              0              0              
test.t_message_list                                         190313 9:30:16                307544         0              0              
test.t_message_content_lately                               190313 9:30:16                307544         0              0              
test.admin_user                                             190308 11:51:50               3              0              3              


Trans(total)                                                Insert(s)                     Update(s)      Delete(s)      
312619                                                      616270                        4565           225            
root /data/git/analysis_binlog/res >> 
```

> 测试过程发现，一个1GB大小的binlog，大概需要2min左右即可。


# analysis_binlog

#### 介绍
分析binlog工具，现有功能：

- 1、基于业务表分析统计各个表的dml的次数。
- 2、各个业务表的最后访问时间。
- 3、各dml总的dml。
- 4、该binlog的事务总数。
- 5、基于业务表的binlog to sql(功能已经实现，但还在测试阶段)。
- 6、其他功能敬请期待。

> git 连接：https://gitee.com/mo-shan/analysis_binlog

> 实现原理参考的是percona的分析脚本：https://www.percona.com/blog/2015/01/20/identifying-useful-information-mysql-row-based-binary-logs/

### 功能改进
- 1、percona官方提供的分析命令是串行化处理，analysis_binlog脚本将统计功能放在内存中且并行处理。
- 2、更加详细的统计信息
- 3、格式化的输出。


### 下面是一个对比demo：

- 1、percona官方提供的分析命令

 (1)统计

```
[root@xxxxxx test]# cat summarize_binlogs.sh 
/usr/local/mysql/bin/mysqlbinlog --base64-output=decode-rows -vv /mfw_rundata/backup/db/mysql/binlog/192.168.2.227_3306/0022273306-mysql-bin.003293 |awk 'BEGIN {s_type=""; s_count=0;count=0;insert_count=0;update_count=0;delete_count=0;flag=0;} \
{if(match($0, /#19.*Table_map:.*mapped to number/)) {printf "Timestamp : " $1 " " $2 " Table : " $(NF-4); flag=1} \
else if (match($0, /(### INSERT INTO .*..*)/)) {count=count+1;insert_count=insert_count+1;s_type="INSERT"; s_count=s_count+1;}  \
else if (match($0, /(### UPDATE .*..*)/)) {count=count+1;update_count=update_count+1;s_type="UPDATE"; s_count=s_count+1;} \
else if (match($0, /(### DELETE FROM .*..*)/)) {count=count+1;delete_count=delete_count+1;s_type="DELETE"; s_count=s_count+1;}  \
else if (match($0, /^(# at) /) && flag==1 && s_count>0) {print " Query Type : "s_type " " s_count " row(s) affected" ;s_type=""; s_count=0; }  \
else if (match($0, /^(COMMIT)/)) {print "[Transaction total : " count " Insert(s) : " insert_count " Update(s) : " update_count " Delete(s) : " \
delete_count "] \n+----------------------+----------------------+----------------------+----------------------+"; \
count=0;insert_count=0;update_count=0; delete_count=0;s_type=""; s_count=0; flag=0} } '
[root@xxxxxx test]# cat test.sh 
bash summarize_binlogs.sh | grep Table |cut -d':' -f5| cut -d' ' -f2 | sort | uniq -c | sort -nr > res.log
bash summarize_binlogs.sh | grep -E "DELETE" |cut -d':' -f5| cut -d' ' -f2 | sort | uniq -c | sort -nr > delete.log
bash summarize_binlogs.sh | grep -E "UPDATE" |cut -d':' -f5| cut -d' ' -f2 | sort | uniq -c | sort -nr > update.log
bash summarize_binlogs.sh | grep -E "INSERT" |cut -d':' -f5| cut -d' ' -f2 | sort | uniq -c | sort -nr > insert.log
[root@xxxxxx test]# 
[root@xxxxxx test]# time bash test.sh 

real    6m38.464s
user    9m15.922s
sys     0m18.562s
[root@xxxxxx test]#

```

 (2)结果展示

```
[root@xxxxxx test]# head -10 res.log
 177850 `mhjendwo`.`meffhde_fub_iwem`
 172769 `mhjendwo`.`meffhde_pub_hecohd`
 136908 `mhjendwo`.`fhlef_ef_phoducw_middle`
  52724 `mhjendwo`.`fhlef_counweh`
  49294 `mhjendwo`.`fhlef_phodheff_bhh`
  15974 `mhjendwo`.`meffhde_fehvice_fend_plhn`
   7273 `mhjendwo`.`w_fhlef_difwhibuwion_phomowion_expenfef`
   5605 `mhjendwo`.`fhlef_whip_fwhucw_jlidhw_ohidin`
   5323 `mhjendwo`.`meffhde_fehvice_fend_fwhwifwicf`
   5321 `mhjendwo`.`meffhde_fehvice_fend_dhwh`

[root@xxxxxx test]# head -10 delete.log        
  51744 `mhjendwo`.`meffhde_fub_iwem`
  44397 `mhjendwo`.`meffhde_pub_hecohd`
    314 `mhjendwo`.`fhlef_jhvohiwef`
     38 `mhjendwo`.`fhlef_limiw`
     12 `mhjendwo`.`fhlef_exp_hepohw_imhde`
      9 `mhjendwo`.`fhlef_felj_owh_quowe`
      6 `mhjendwo`.`fhlef_whhvelleh_wemplhwe_mhp`
      4 `mhjendwo`.`wickewf_fku_poi_iwem`
      1 `mhjendwo`.`w_fhlef_difwhibuwion_fhop_phoducw`
      1 `mhjendwo`.`fhlef_doodf_dhhjw`

[root@xxxxxx test]# head -10 update.log          
 136902 `mhjendwo`.`fhlef_ef_phoducw_middle`
  52687 `mhjendwo`.`fhlef_counweh`
  49294 `mhjendwo`.`fhlef_phodheff_bhh`
  10652 `mhjendwo`.`meffhde_fehvice_fend_plhn`
   7273 `mhjendwo`.`w_fhlef_difwhibuwion_phomowion_expenfef`
   5388 `mhjendwo`.`fhlef_whip_fwhucw_jlidhw_ohidin`
   5323 `mhjendwo`.`meffhde_fehvice_fend_fwhwifwicf`
   4271 `mhjendwo`.`cufwomize_fcheme_dhhjw`
   2149 `mhjendwo`.`dehlf_moniwoh_pufh`
   1308 `mhjendwo`.`fhlef_limiw`

[root@xxxxxx test]# head -10 insert.log           
 128372 `mhjendwo`.`meffhde_pub_hecohd`
 126106 `mhjendwo`.`meffhde_fub_iwem`
   5322 `mhjendwo`.`meffhde_fehvice_fend_plhn`
   5321 `mhjendwo`.`meffhde_fehvice_fend_dhwh`
    850 `mhjendwo`.`fhlef_jhvohiwef`
    794 `mhjendwo`.`dehlf_pufh_injo`
    794 `mhjendwo`.`dehlf_moniwoh_pufh`
    790 `mhjendwo`.`dehlf_moniwoh_ohdeh_dewhil`
    570 `mhjendwo`.`fhlef_limiw`
    314 `mhjendwo`.`fhlef_jhvohiwef_del`
[root@xxxxxx test]# 
```

- 2、analysis_binlog脚本分析工具

 (1)统计

```
[root@xxxxxx analysis_binlog]# time ./bin/analysis_binlog -bfile=/xxxx/backup/db/mysql/binlog/192.168.x.xxxx_3306/00xxxx3306-mysql-bin.003293 -w=1 -t=simple -s=total
[2019-04-22 16:06:39] [INFO] [10.1.1.xx] Analysing -->/mfw_rundata/backup/db/mysql/binlog/192.168.x.xxx_3306/00xxxx3306-mysql-bin.003293
[2019-04-22 16:08:54] [INFO] [10.1.1.xx] Analysis completed -->/mfw_rundata/backup/db/mysql/binlog/192.168.x.xxx_3306/00xxxx3306-mysql-bin.003293



real    2m18.891s
user    2m54.266s
sys     0m5.411s
[root@xxxxxx analysis_binlog]# 

```

 (2)结果展示

```
[root@xxxxxx res]# head -10 00xxxx3306-mysql-bin.003293.res
Table                                                       Last Time                     Insert(s)      Update(s)      Delete(s)      Total(s)       
mhjendwo.meffhde_fub_iwem                                   190421 16:17:35               144215         0              51744          195959         
mhjendwo.meffhde_pub_hecohd                                 190421 16:17:35               128372         0              44397          172769         
mhjendwo.fhlef_ef_phoducw_middle                            190421 16:17:35               6              136902         0              136908         
mhjendwo.fhlef_counweh                                      190421 16:17:35               37             52687          0              52724          
mhjendwo.fhlef_phodheff_bhh                                 190421 16:17:35               0              49294          0              49294          
mhjendwo.meffhde_fehvice_fend_plhn                          190421 16:17:34               5322           10652          0              15974          
mhjendwo.w_fhlef_difwhibuwion_phomowion_expenfef            190421 16:17:35               0              7273           0              7273           
mhjendwo.fhlef_whip_fwhucw_jlidhw_ohidin                    190421 16:17:26               217            5388           0              5605           
mhjendwo.meffhde_fehvice_fend_fwhwifwicf                    190421 16:17:34               0              5323           0              5323          


[root@xxxxxx res]# tail -10 00xxxx3306-mysql-bin.003293.res    
mhjendwo.lochldehlf_nowice                                  190421 16:16:57               1              0              0              1              
mhjendwo.dehlf_wickew_ohdeh_vifiwoh                         190421 16:10:54               1              0              0              1              
mhjendwo.cufwomize_fcheme_wemplhwe_bhfe                     190421 16:11:31               1              0              0              1              
mhjendwo.cufwomize_ef_fcheme_wemplhwe_middle                190421 16:11:31               1              0              0              1              
mhjendwo.cufwomize_difwhibuwe                               190421 16:14:48               1              0              0              1                


Trans(total)                                                Insert(s)                     Update(s)      Delete(s)    
648497                                                      289939                        279808         96517          
[root@xxxxxx res]# 
```